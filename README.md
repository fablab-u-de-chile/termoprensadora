# Termo-Compactadora.

 Prensa para termo-compactar diferentes tipos de materiales , principalmente agro materiales basados en celulosa, pectinas, alginatos, plásticos basados en almidón, etc.

### ¿Cómo funciona?

Funciona con una prensa hidráulica de 20 toneladas, en la cual se montan las matrices fabricadas en acero las que dan forma al material prensado.

En la base de la prensa se monta el cuerpo de la matriz y en el pistón se monta el émbolo que es la pieza que se desplaza y genera la compactación.

Algunos materiales para poder compactarse necesitan que la prensa tenga calor, para lograrlo la matriz cuenta con calefactores de 300 W para compactar en rangos de temperaturas que van de 0 grados a 450 grados máximo que entregan los calefactores.

<img src="images/img01.jpg" width="300">

### ¿Cómo fue construida?

La termo-compactadora se compone de las siguientes partes:

- Una prensa hidráulica de 20 toneladas
- Una matriz hecha con duraluminio y acero   mecanizadas con un centro de mecanizado CNC
- 4 calefactores de cartucho
- Un control de temperartura PID
- Una termocupla tipo J
- Un interruptor y un relé de estado solido.


#### Lista de componentes electricas

Links de referencia.

| componentes | Link de referencia |
|-------------------|--------------------|
|Calefactor cartucho ø8mm x 60mm 300W 230v | https://www.comind.cl/calefactores-de-cartucho/ |
|Calefactor cartucho ø10mm x 100mm 300W 230v | https://www.comind.cl/calefactores-de-cartucho/ |
|AN708 Series intelligent temperature controller| https://www.sommy.com.cn/up_pic/file/20210510134855505550.pdf |
| Termocupla tipo J M6 | https://es.aliexpress.com/item/1005002537307049.html |
| Relé de estado solido SSR-40 DA 24 A | https://www.amazon.com/-/es/Inkbird-estado-controlador-temperatura-termostato/dp/B00HV974KC |


### Partes de la matriz

<img src="images/img02.png" width="300">     <img src="images/img03.png" width="300">

### Esquema de conexiones electricas

<img src="/images/img04.png" width="600">

### Video del funcionamiento y resultados

[![video_proceso](https://img.youtube.com/vi/hYMQQR6m5EA/0.jpg)](https://www.youtube.com/watch?v=hYMQQR6m5EA)

### Informe de experimentación con agromateriales.
